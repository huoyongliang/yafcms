<?php
/**
 * @name Base
 * @desc 基础公共类
 */
class Base extends \Yaf\Controller_Abstract
{
    public function init()
    {
        $url = $this->getRequest()->getMethod().':/gincms/'.$this->getRequest()->getControllerName().'/'.$this->getRequest()->getActionName();
        //登录校验和获取验证码无需鉴权
        if (!($url == 'GET:/gincms/Ajax/getcaptcha' || $url == 'POST:/gincms/Ajax/getuserlogin')) {
            if (intval($_SESSION['uid']) === 0) {
                json(-1, '鉴权失效，请重新登录!');
                return false;
                exit();
            }
            if (strpos($url,'/gincms/Ajax/')===false){
                $urlarr = explode('/',$url);
                array_pop($urlarr);
                $url = implode('/',$urlarr).'/';
                $this->CheckAuthRule($url,$_SESSION['group_id']);
            }
        }
    }

    //根据受访url和用户组,进行节点鉴权
    private function CheckAuthRule($url, $group_id)
    {
        $sqlstr = sprintf("select count(*) as count from fa_auth_rule r INNER JOIN fa_auth_group g on FIND_IN_SET(r.id, g.rules) and r.`name`='%s' and ismenu=2 and r.`status`='normal' where g.id=%d ", $url, $group_id);
        $model = new \App\Models\Model();
        $result = $model->query($sqlstr);
        if ($result[0]['count']>0){
            return;
        } else {
            json(-1, '(╥╯^╰╥) 当前访问节点未授权!');
            exit();
        }
    }

    //信息保存
    public function saveAction()
    {
        try {
            $model = $this->model;
            if (!$model->create(input(""))){
                $errtips = $model->getError();
                json(-1,$errtips);
            }else{
                $id = input('id');
                if ($id>0){
                    $result = $model->where($model->getPk()." ='{$id}' ")->save();
                } else {
                    $result = $model->add();
                }
                //echo $model->getlastsql();
                if ($result===false){
                    json(-1,'保存信息失败');
                } else {
                    json(0,'保存信息成功');
                }
            }
        } catch(Exception $e){
            json(-1,$e->getMessage());
        }
    }

    //信息排序修改
    public function changeAction()
    {
    	$id = input('id',0);
    	$data = input("");
        unset($data['id']);
        $model = $this->model;
        if (!$model->create($data)){
            $errtips = $model->getError();
            json(-1,$errtips);
            return false;
        }
        $re = $model->where(" id='{$id}' ")->save();
        if($re){
            json(0,'修改信息成功');
        } else {
            json(-1,'修改信息失败');
        }
        
    }
    
    //信息删除
    public function destroyAction()
    {
        $id  = input('id',0);
		$model = $this->model;
		$res = $model->delete($id);
        json(0,'删除信息成功');
    }

    //文件上传
    public function uploadAction()
    {
        $upload = new spUploadFile();
        $url = $upload->upload_file($_FILES['file'],"jpg|png|gif|jpeg",'');
        $data = ['src'=>$url];
        json(0,'上传文件成功',$data);
    }
}
