<?php
use App\Models\Group;
/**
 * @name AuthgroupController
 * @desc 角色管理
 * @see http://127.0.0.1/gincms/authgroup/querylist
 */
class AuthgroupController extends Base
{
    protected $model = null;
    
    public function init()
    {
        $this->model = new Group();
    }

    //列表数据
    public function querylistAction()
    {
        $query   = input('query');
        $page    = input('page',0);
        $pagesize= input('limit',10);
        $where = array();
        if ($query<>''){
            $where['name'] = array('like',"%{$query}%");
        }
        $model = $this->model;
        $count = $model->where($where)->count();
        $datalist = $model->where($where)->order('id desc')->page($page, $pagesize)->select();
        json(0,'数据列表',$datalist,$count);
    }
}
