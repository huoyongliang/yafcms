<?php
use App\Models\Rule;
/**
 * @name AuthruleController
 * @desc 权限节点管理
 * @see http://127.0.0.1/Index/Rule/index
 */
class AuthruleController extends Base
{
    protected $model = null;
    
    public function init()
    {
        $this->model = new Rule();
    }

    //列表页面
    public function querylistAction()
    {
        $model = $this->model;
        $datalist = $model->select();
        if ($datalist){
            foreach($datalist as $key=>$item){
                $datalist[$key]['id']  = intval($item['id']);
                $datalist[$key]['pid'] = intval($item['pid']);
            }
        }
        json_data($datalist);
    }
    
}
