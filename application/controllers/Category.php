<?php
use App\Models\Category;
/**
 * @name CategoryController
 * @desc 分类管理
 * @see http://127.0.0.1/Index/Category/index
 */
class CategoryController extends Base
{
    protected $model = null;
    
    public function init()
    {
        $this->model = new Category();
    }

    //列表页面
    public function querylistAction()
    {
        $model = $this->model;
        $datalist = $model->select();
        if ($datalist){
            foreach($datalist as $key=>$item){
                $datalist[$key]['id']  = intval($item['id']);
                $datalist[$key]['pid'] = intval($item['pid']);
            }
        }
        json_data($datalist);
    }
}
