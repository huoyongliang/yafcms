<?php
/**
 * @name AjaxController
 * @desc Ajax方式获取json数据
 * @see http://127.0.0.1/Index/User/index
 */
class AjaxController extends Base
{
    protected $model = null;
    
    //获取导航菜单
    public function getnavAction()
    {
        $bignav = ["content"=>1, "member"=>2, "system"=>3, "other"=>4];
        $where = [
            'ismenu'        => 1,
            'r.`status`'   => 'normal',
            'g.id'          => $_SESSION['group_id'],
        ];
        $model = new \App\Models\Rule();
        $result = $model->table('fa_auth_rule as r')->join('fa_auth_group as g',"INNER")->field("r.pid,r.`name` as href,r.title,r.icon")->where('FIND_IN_SET(r.id, g.rules)')->where($where)->select();
        $navs = [];
        if ($result){
            foreach ($bignav as $key=>$val){
                foreach ($result as $item){
                    if ($val==$item['pid']){
                        $navs[$key][] = $item;
                    }
                }
            }
        }
        json(0,'获取菜单导航成功',$navs);
    }

    //校验用户登录
    public function getuserloginAction()
    {
        $username = input("username",'');
        $userpwd  = input("password",'');
        $vcode    = input("verfiycode",'');

        $model = new \App\Models\Admin();
        try {
            $result = $model->checkLogin($username,$userpwd,$vcode);
            if ($_SESSION['uid']>0){
                json(0,'登录成功',$result);
            }
        } catch(Exception $e){
            json(-1,$e->getMessage());
        }
        return false;
    }

    //返回当前登录用户信息
    public function getuserinfoAction()
    {
        if ($_SESSION['uid']>0){
            $data = [
                'id'        => $_SESSION['uid'],
                'username'  => $_SESSION['username'],
                'nickname'  => $_SESSION['nickname'],
                'email'     => $_SESSION['email'],
                'avatar'    => $_SESSION['avatar'],
                'group_id'  => $_SESSION['group_id'],
            ];
                json(0,'获取用户信息成功',$data);
        }else{
            json(-1,"获取用户信息失败，请重新登录");
        }
    }

    //验证码图片
    public function getcaptchaAction()
    {
        $captcha = new Captcha();
        $captcha->generate(4,146,48);
        return false;
    }

    //退出登录
    public function getloginoutAction()
    {
        unset($_SESSION['uid']);
        json(0,'用户退出登录成功……');
    }

    //内容统计
    public function getdatastatsAction()
    {
        $where = ['`status`' => 'normal'];
        $model = new \App\Models\Model();
        $article_total = $model->table("fa_article")->where($where)->count();
        $category_total = $model->table("fa_category")->where($where)->count();
        $order_total = $model->table("fa_order")->where($where)->count();
        $admin_total = $model->table("fa_admin")->where($where)->count();
        $group_total = $model->table("fa_auth_group")->where($where)->count();
        $result = [
            'article_total'  => $article_total,
            'category_total' => $category_total,
            'order_total'    => $order_total,
            'admin_total'    => $admin_total,
            'group_total'    => $group_total,
        ];
        json(0,'内容数量统计信息',$result);
    }

    //分类列表
    public function getcategoryAction()
    {
        $model = new \App\Models\Category();
        $where = ['`status`' => 'normal'];
        $types = input("type","");
        if ($types!="") {
            $where['`type`'] = $types;
        }
        $result = $model->field('id,name,pid')->where($where)->order("weigh desc")->select();
        foreach ($result as $key=>$item){
            $result[$key]['id']  = intval($item['id']);
            $result[$key]['pid'] = intval($item['pid']);
        }
        json(0,"分类列表",$result);
    }

    //用户分组列表
    public function getauthgroupAction()
    {
        $model = new \App\Models\Group();
        $where = ['`status`' => 'normal'];
        $result = $model->field('id,name')->where($where)->order("id asc")->select();
        json(0,"用户分组列表",$result);
    }

    //权限节点列表
    public function getauthruleAction()
    {
        $model = new \App\Models\Rule();
        $where = ['`status`' => 'normal'];
        $result = $model->field('id,title as `name`,pid')->where($where)->select();
        foreach ($result as $key=>$item){
            $result[$key]['id']  = intval($item['id']);
            $result[$key]['pid'] = intval($item['pid']);
        }
        json(0,"权限节点列表",$result);
    }

    //更新用户信息
    public function profileAction()
    {
        $model = new \App\Models\Admin();
        $data = input("");
        if ($data['password']<>""){
            $salt = rand(1000,9999);
            $data['password'] = mymd5($data['password'],$salt);
            $data['salt']     = $salt;
        }
        if (!$model->create($data)){
            $errtips = $model->getError();
            json(-1,$errtips);
            return false;
        }
        $re = $model->where(['id'=>$_SESSION['uid']])->save();
        if($re){
            json(0,'修改信息成功');
        } else {
            json(-1,'修改信息失败');
        }
    }
    
}
