<?php
use App\Models\Order;
/**
 * @name OrderController
 * @desc 文章管理
 * @see http://127.0.0.1/Index/Order/index
 */
class OrderController extends Base
{
    protected $model = null;
    
    public function init()
    {
        parent::init();
        $this->model = new Order();
    }

    //列表数据
    public function querylistAction()
    {
        $query   = input('query');
        $page    = input('page',0);
        $pagesize= input('limit',10);
        $filter  = input('filterSos',"");
        $columns = input('columns',"");
        if ($columns<>""){
            //返回各个列的字典值
            $this->columnDict($columns);
            exit();
        }
        $filter  = htmlspecialchars_decode($filter);
        $filterArr = json_decode($filter, true);
        $where = "1=1 ";
        $where = $this->filter($filterArr, $where);
        //echo $where;
        //exit();
        //$where = array();
		//if ($query<>''){
		//	$where['title'] = array('like',"%{$query}%");
        //}
        $model = $this->model;
        $count = $model->where($where)->count();
        $datalist = $model->where($where)->order('id desc')->page($page, $pagesize)->select();
        // echo $model->getlastsql();
        // var_dump($datalist);
        json(0,'数据列表',$datalist,$count);
    }

    //筛选条件
    public function filter($arr,$filterSql)
    {
        foreach ($arr as $item){
            $filterSql = $this->subfilter($item,$filterSql);
        }
        return $filterSql;
    }

    //循环的子条件
    public function subfilter($item,$filterSql)
    {
        if (empty($item)) return "";
        if (!endsWith($filterSql, "(") && !endsWith($filterSql, "WHERE")) {
            $filterSql .= isset($item['prefix']) ? " " . $item['prefix'] : " and" ;
        }
        switch ($item['mode']){
            case 'group':
                $filterSql .= " (";
                if ($item['children']){
                    $filterSql = $this->filter($item['children'], $filterSql);
                }else {
                    $filterSql .= " 1=1";
                }
                $filterSql .= " )";
                break;
            case "in":
                if (empty($item['values'])){
                    $filterSql .= " 1=1";
                    break;
                }
                switch ($item['type']){
                    //暂时不对date做特殊处理
                    case 'date':
                    default:
                        $delimiter = isset($item['split']) ? $item['split'] : "','";
                        $filterSql .= " `".$item['field']."` in ('". implode($delimiter,$item['values']) ."')";
                        break;
                }
                break;
            case 'condition':
                if ($item['value']==""){
                    $filterSql .= " 1=1";
                    break;
                }
                $filterSql .= " `".$item['field']."`";
                switch ($item['type']){
                    case 'eq':
                        $filterSql .= " = '".$item['value']."'";
                        break;
                    case 'ne':
                        $filterSql .= " != '".$item['value']."'";
                        break;
                    case 'gt':
                        $filterSql .= " > '".$item['value']."'";
                        break;
                    case 'ge':
                        $filterSql .= " >= '".$item['value']."'";
                        break;
                    case 'lt':
                        $filterSql .= " < '".$item['value']."'";
                        break;
                    case 'le':
                        $filterSql .= " <= '".$item['value']."'";
                        break;
                    case 'contain':
                        $filterSql .= " like '%".$item['value']."%'";
                        break;
                    case 'notContain':
                        $filterSql .= " not like '%".$item['value']."%'";
                        break;
                    case 'start':
                        $filterSql .= " like '".$item['value']."%'";
                        break;
                    case 'end':
                        $filterSql .= " like '%".$item['value']."'";
                        break;
                    case 'null':
                        $filterSql .= " is null";
                        break;
                    case 'notNull':
                        $filterSql .= " is not null";
                        break;
                }
                break;
            case "date":
                if ($item['value']==""){
                    $filterSql .= " 1=1";
                    break;
                }
                $filterSql .= " `".$item['field']."`";
                switch ($item['type']){
                    case 'yesterday':
                        $filterSql .= " between DATE_ADD(curdate(),INTERVAL -1 DAY) and DATE_ADD(curdate(),  INTERVAL -1 second)";
                        break;
                    case 'thisWeek':
                        $filterSql .= " between date_add(curdate(), interval - weekday(curdate()) day) and date_add(date_add(curdate(), interval - weekday(curdate())+7 day), interval -1 second)";
                        break;
                    case 'lastWeek':
                        $filterSql .= " between date_add(curdate(), interval - weekday(curdate())-7 day) and date_add(date_add(curdate(), interval - weekday(curdate()) day), interval -1 second)";
                        break;
                    case 'thisMonth':
                        $filterSql .= " between date_add(curdate(), interval - day(curdate()) + 1 day) and DATE_ADD(last_day(curdate()), interval 24*60*60-1 second)";
                        break;
                    case 'thisYear':
                        $filterSql .= " between date_sub(curdate(),interval dayofyear(now())-1 day) and str_to_date(concat(year(now()),'-12-31 23:59:59'), '%Y-%m-%d %H:%i:%s')";
                        break;
                    case 'specific':
                        $filterSql .= " between '{$item['value']}' and '{$item['value']} 23:59:59'";
                        break;
                }
                break;
        }

        return $filterSql;
    }

    //返回各个列的字典值
    function columnDict($columns){
        $columns  = htmlspecialchars_decode($columns);
        $columns  = json_decode($columns,true);
        $result = array();
        $model = $this->model;
        foreach ($columns as $field){
            $datalist = $model->distinct(true)->field($field)->select();
            //echo $model->getlastsql();
            //dump($datalist);
            $temp = array();
            foreach ($datalist as $item){
                 $temp[] = $item[$field];
            }
            $result[$field] = $temp;
        }
        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        return;
    }
    
}
