<?php
use App\Models\Article;
use App\Models\Category;
/**
 * @name ArticleController
 * @desc 文章管理
 * @see http://127.0.0.1/Index/Article/index
 */
class ArticleController extends Base
{
    protected $model = null;
    
    public function init()
    {
        parent::init();
        $this->model = new Article();
    }

    //列表数据
    public function querylistAction()
    {
        $query   = input('query');
        $page    = input('page',0);
        $pagesize= input('limit',10);
        $where = array();        
		if ($query<>''){
			$where['title'] = array('like',"%{$query}%");
        }
        $model = $this->model;
        $count = $model->where($where)->count();
        $datalist = $model->where($where)->order('is_top desc,id desc')->page($page, $pagesize)->select();
        //echo $model->getlastsql();
        // var_dump($datalist);
        $model = new Category();
        $category = $model->where("`status`='normal' ")->getField('id,name');
        if ($datalist){
            foreach($datalist as $key=>$item){
                $datalist[$key]['catename'] = $category[$item['article_cate_id']];
                $datalist[$key]['content'] = htmlspecialchars_decode($item['content']);
            }
        }
        json(0,'数据列表',$datalist,$count);
    }
    
}
