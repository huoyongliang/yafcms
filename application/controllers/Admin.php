<?php
use App\Models\Admin;
/**
 * @name UserController
 * @desc 用户管理
 * @see http://127.0.0.1/Index/User/index
 */
class AdminController extends Base
{
    protected $model = null;
    
    public function init()
    {
        $this->model = new Admin();
    }

    //列表数据
    public function querylistAction()
    {
        $query   = input('query');
        $page    = input('page',0);
        $pagesize= input('limit',10);
        $where = array();
		if ($query<>''){
			$where['username|nickname'] = array('like',"%{$query}%");
        }
        $model = $this->model;
        $count = $model->where($where)->count();
        $datalist = $model->field('id,username,nickname,avatar,email,logintime,createtime,`status`,group_id')->where($where)->order('id desc')->page($page, $pagesize)->select();
        json(0,'数据列表',$datalist,$count);
    }

    //信息保存
    public function saveAction()
    {
        try {
            $model = $this->model;
            $data = input("");
            if ($data['password']<>""){
                $salt = rand(1000,9999);
                $data['password'] = mymd5($data['password'],$salt);
                $data['salt']     = $salt;
            }
            if (!$model->create($data)){
                $errtips = $model->getError();
                json(-1,$errtips);
            }else{
                $id = input('id');
                if ($id>0){
                    $result = $model->where($model->getPk()." ='{$id}' ")->save();
                } else {
                    $result = $model->add();
                }
                //echo $model->getlastsql();
                if ($result===false){
                    json(-1,'保存信息失败');
                } else {
                    json(0,'保存信息成功');
                }
            }
        } catch(Exception $e){
            json(-1,$e->getMessage());
        }
    }
}
