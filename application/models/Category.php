<?php
namespace App\Models;

class Category extends Model
{
    protected $tableName = 'category';
    protected $pk = 'id';
    
    protected $_validate = array(
            array('type','require','栏目类型不能为空'),
            array('name','require','栏目名称不能为空'),
            array('name','','栏目名称已经存在',0,'unique',1), // 在新增的时候验证name字段是否唯一
        );
    
    //自动完成
    protected $_auto = array (
            array('createtime','gettime',1,'callback'),
            array('updatetime','gettime',3,'callback'),
        );
    
    public $cate_type = ['page'=>'页面','article'=>'文章','link'=>'链接','product'=>'产品'];

}
