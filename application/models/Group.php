<?php
namespace App\Models;

class Group extends Model
{
    protected $tableName = 'auth_group';
    protected $pk = 'id';

    protected $_validate = array(
            array('name','require','角色名称不能为空'),
        );        

    //自动完成
    protected $_auto = array (
            array('createtime','gettime',1,'callback'),
            array('updatetime','gettime',3,'callback'),
        );

    protected $_map = array(
            // 'name' =>'username', // 把表单中name映射到数据表的username字段
            // 'mail'  =>'email', // 把表单中的mail映射到数据表的email字段
        );    	

    

}
