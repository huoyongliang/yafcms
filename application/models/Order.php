<?php
namespace App\Models;

class Order extends Model
{
    protected $tableName = 'order';
    protected $pk = 'id';
    
    protected $_validate = array(
            array('contacts','require','联系人不能为空'),
            array('telephone','require','电话不能为空'),
        );
    
    //自动完成
    protected $_auto = array (
            array('status','normal'),
            array('createtime','gettime',1,'callback'),
        );
    
}
