<?php
namespace App\Models;

class Rule extends Model
{
    protected $tableName = 'auth_rule';
    protected $pk = 'id';

    protected $_validate = array(
            array('name','require','URL不能为空'),
            array('title','require','规则名称不能为空'),
        );        

    //自动完成
    protected $_auto = array (
            array('createtime','gettime',1,'callback'),
            array('updatetime','gettime',3,'callback'),
        );

    protected $_map = array(
            // 'name' =>'username', // 把表单中name映射到数据表的username字段
            // 'mail'  =>'email', // 把表单中的mail映射到数据表的email字段
        );
    


}
