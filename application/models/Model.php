<?php
namespace App\Models;

class Model extends \Model
{
	protected $tablePrefix = 'fa_';
	
	function getuid() {
		return $_SESSION['uid'];
	}
	//获取当前时间戳
	function gettime() {
		return time();
	}
	
	//获取当前系统时间
	function getdatatime() {
		return date('Y-m-d H:i:s');
	}
    
    //获取当前系统日期
	function getdata() {
		return date('Y-m-d');
	}
}
