<?php
namespace App\Models;

class Article extends Model
{
    protected $tableName = 'article';
    protected $pk = 'id';
    
    protected $_validate = array(
            array('article_cate_id','require','栏目ID不能为空'),
            array('title','require','文章名称不能为空'),
            array('content','require','文章内容不能为空'),        
        );
    
    //自动完成
    protected $_auto = array (
            array('views','0'),
            array('admin_id','getuid',1,'callback'),
            array('createtime','gettime',1,'callback'),
            array('updatetime','gettime',3,'callback'),
        );
    
}
