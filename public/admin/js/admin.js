/*
 * +----------------------------------------------------------------------
 * | 通用的方法，比如ajax提交数据等
 * | 基于Layui http://www.layui.com/
 * +----------------------------------------------------------------------
 */

layui.define(['jquery', 'form', 'layer', 'element'], function(exports) {
	var $ = layui.jquery,
		form = layui.form,
		layer = layui.layer,
		element = layui.element;
	/*
            参数解释：
            url     请求的url
            type    get/post/put/delete/patch
            data    需要传输的数据
            successfun ajax执行成功之后执行的函数
        */
	window.ZjqAdminAjax = function(url, type, data, successfun) {
		var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
		$.ajax({
			url: url,
			data: data,
			type: type,
			dataType: "json",
			success: function (res) {
				if (0 == res.code){
					top.layer.msg(res.msg, {icon: 6, time:1000},successfun);
				} else {
					top.layer.msg(res.msg, {icon: 2, time:3000});
				}
			},
			error: function (res) {
				console.info(res);
				top.layer.msg(res.status+"："+res.statusText, {icon: 2, time:3000});
			}
		});
	};


	/*
	    参数解释：
	    url     请求的url
	    data    需要传输的数据    
	    successfun ajax执行成功之后执行的函数    
	*/
	window.YafAdminAjax = function(url, data, successfun) {
		$.ajax({
			url: url,
			data: data,
			type: "post",
			dataType: "json",
			success: function (data) {				
				if (data.code==0){
					layer.msg(data.msg, {icon: 6, time:1000},successfun);
				} else {
					layer.msg(data.msg, {icon: 2, time:3000});	
				}
			},
			error: function (data) {
				layer.msg(data.msg, {icon: 2, time:3000});
			}
		});
	}

	//删除选中数据
	window.YafAdminDelAll = function(url) {
		var checkStatus = layui.table.checkStatus('ListTable')
		,obj = checkStatus.data;
		if (obj.length >0 ){
			var arr = [];
			for(index in obj ) {
				arr.push(obj[index].id);
			}
			var ids = arr.join()
			layer.confirm('确认要删除选中的数据吗？', {icon: 3, title: '提示信息'}, function(index) {
				var successfun = function(){
                    layer.close(index);
                    layui.table.reload('ListTable');
				}
				YafAdminAjax(url,{id:ids},successfun);
			});
        } else {
            layer.msg("请选择需要删除的数据");
        }

    }

	//删除选中数据
	window.YafAdminDel = function(url,id) {
		layer.confirm('确定删除此数据吗？', {icon: 3, title: '提示信息'}, function(index) {
			var successfun = function(){
                layer.close(index);
                layui.table.reload('ListTable');
			}
            YafAdminAjax(url,{id:id},successfun);
		});
	}

	/*
	 * @todo 弹出层，弹窗方法
	 * layui.use 加载layui.define 定义的模块，当外部 js 或 onclick调用 use 内部函数时，需要在 use 中定义 window 函数供外部引用
	 * http://blog.csdn.net/xcmonline/article/details/75647144 
	 */
	/*
	    参数解释：
	    title   标题
	    url     请求的url
	    id      需要操作的数据id
	    w       弹出层宽度（缺省调默认值）
	    h       弹出层高度（缺省调默认值）
	*/
	window.YafAdminShow = function(title, url, w, h) {
		if(title == null || title == '') {
			title = false;
		};
		if(url == null || url == '') {
			url = "404.html";
		};
		if(w == null || w == '') {
			w = ($(window).width() * 0.9);
		};
		if(h == null || h == '') {
			h = ($(window).height() - 50);
		};
		layer.open({
			type: 2,
			area: [w + 'px', h + 'px'],
			fix: false, //不固定
			maxmin: true,
			shadeClose: true,
			shade: 0.4,
			title: title,
			content: url
		});
	}

	// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
	// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
	// 例子：   
	//Format("2016-10-04 8:9:4.423","yyyy-MM-dd hh:mm:ss.S") ==> 2016-10-04 08:09:04.423   
	// Format("1507353913000","yyyy-M-d h:m:s.S")      ==> 2017-10-7 13:25:13.0  
	window.DateTimeFormat = function(datetime,fmt) {
		if (parseInt(datetime)==datetime) {
		if (datetime.length==10) {
			datetime=parseInt(datetime)*1000;
		} else if(datetime.length==13) {
			datetime=parseInt(datetime);
		}
		}
		datetime=new Date(datetime);
		var o = {
		"M+" : datetime.getMonth()+1,                 //月份   
		"d+" : datetime.getDate(),                    //日   
		"h+" : datetime.getHours(),                   //小时   
		"m+" : datetime.getMinutes(),                 //分   
		"s+" : datetime.getSeconds(),                 //秒   
		"q+" : Math.floor((datetime.getMonth()+3)/3), //季度   
		"S"  : datetime.getMilliseconds()             //毫秒   
		};   
		if(/(y+)/.test(fmt))   
		fmt=fmt.replace(RegExp.$1, (datetime.getFullYear()+"").substr(4 - RegExp.$1.length));   
		for(var k in o)   
		if(new RegExp("("+ k +")").test(fmt))   
		fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
		return fmt;
	};

	window.SizeFormat = function(size) {
		if (!size)
			return "";
		var num = 1024.00; //byte
		if (size < num)
			return size + "B";
		if (size < Math.pow(num, 2))
			return (size / num).toFixed(2) + "K"; //kb
		if (size < Math.pow(num, 3))
			return (size / Math.pow(num, 2)).toFixed(2) + "M"; //M
		if (size < Math.pow(num, 4))
			return (size / Math.pow(num, 3)).toFixed(2) + "G"; //G
		return (size / Math.pow(num, 4)).toFixed(2) + "T"; //T
	};


	exports('admin', {});
});