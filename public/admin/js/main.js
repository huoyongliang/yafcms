layui.config({
    base : "../js/"
});
layui.use(['form','element','layer','jquery','laytpl','admin'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laytpl = layui.laytpl,
        element = layui.element;
        $ = layui.jquery;
    //icon动画
    $(".panel a").hover(function(){
        $(this).find(".layui-anim").addClass("layui-anim-scaleSpring");
    },function(){
        $(this).find(".layui-anim").removeClass("layui-anim-scaleSpring");
    })
    $(".panel a").click(function(){
        parent.addTab($(this));
    })

    //系统概览
    $.ajax({
        url : "/gincms/ajax/getrunstats",
        type : "get",
        dataType : "json",
        success : function(res){
            var getTpl = $("#statstemp").html();
            res.data.memstats = JSON.parse(res.data.memstats);
            laytpl(getTpl).render(res.data, function(html){
                $("#stats").html(html);
            });
        }
    });

    //内容数量统计
    $.ajax({
        url : "/gincms/ajax/getdatastats",
        type : "get",
        dataType : "json",
        success : function(res){
            //console.info(res);
            $(".article_total span").text(res.data.article_total);
            $(".category_total span").text(res.data.category_total);
            $(".order_total span").text(res.data.order_total);
            $(".admin_total span").text(res.data.admin_total);
            $(".group_total span").text(res.data.group_total);
        }
    });


})
