// layui.extend({
//     admin: '../../js/admin'
// });
layui.config({
    base: '../../js/',
})
layui.use(['form','layer','table','laytpl','admin','treeTable'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table,
        treeTable = layui.treeTable;

    var	re = treeTable.render({
        icon: {
            open: 'layui-icon layui-icon-down',
            close: 'layui-icon layui-icon-right',
            left: 16,
        },
        elem: '#infoList',
        url: '/gincms/category/querylist',
        icon_key: 'name',
        is_checkbox: true,
        end: function(e){
            form.render();
        },
        cols: [
            {
                key: 'id',
                title: 'ID',
                width: '50px',
                align: 'center'
            },
            {
                key: 'type',
                title: '分类类型',
                width: '100px',
                align: 'center'
            },
            {
                key: 'name',
                title: '分类名称',
                width: '150px',
                template: function(item){
                    if(item.type == 'article'){
                        return '<span style="color:red;">'+item.name+'</span>';
                    }else if(item.type == 'product'){
                        return '<span style="color:#009688;">'+item.name+'</span>';
                    }else if(item.type == 'page'){
                        return '<span style="color:blue;">'+item.name+'</span>';
                    }else{
                        return item.name;
                    }
                }
            },
            {
                key: 'nickname',
                title: '分类别名',
                width: '150px',
                align: 'left'
            },
            {
                key: 'diyname',
                title: '自定义路径',
                align: 'left'
            },
            {
                key: 'weigh',
                title: '权重',
                width: '100px',
                align: 'center'
            },
            {
                title: '状态',
                width: '150px',
                align: 'center',
                template:function(d){
                    if(d.status == "default"){
                        return '<span class="layui-red">等待审核</span>';
                    } else if(d.status == "draft"){
                        return '<span class="layui-blue">已存草稿</span>';
                    } else if (d.status == "normal"){
                        return '<span class="layui-green">正常</span>';
                    } else {
                        return '<span class="layui-red">未定义</span>';
                    }
                }
            },
            {
                title: '操作',
                width: '200px',
                align: 'center',
                template: function(item){
                    return '<a class="layui-btn layui-btn-xs" lay-filter="edit">编辑</a> <a class="layui-btn layui-btn-xs layui-btn-danger" lay-filter="del">删除</a>';
                }
            }
        ]
    });

    //编辑信息
    function editInfo(edit){
        var index = layui.layer.open({
            title : "编辑数据",
            area: ['90%', '90%'],
            type : 2,
            content : "edit.html",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                //console.info(edit);
                if(edit){
                    body.find("#id").val(edit.id);
                    body.find("#type").val(edit.type);
                    body.find("#pid").val(edit.pid);//
                    body.find(".name").val(edit.name);
                    body.find(".nickname").val(edit.nickname);
                    body.find(".diyname").val(edit.diyname);
                    body.find(".keywords").val(edit.keywords);
                    body.find(".description").val(edit.description);
                    body.find(".weigh").val(edit.weigh);
                    body.find("#status").val(edit.status);
                    form.render();
                }
                setTimeout(function(){
                    layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        });
        //layui.layer.full(index);//全屏
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            //layui.layer.full(index);
        })
    }

    $(".editInfo_btn").click(function(){
        editInfo();
    });

    //批量删除
    $(".delAll_btn").click(function(){
        var ids = treeTable.checked(re).join(',');
        if(ids.length > 0) {
            layer.confirm('确定删除选中的数据？', {icon: 3, title: '提示信息'}, function (index) {
                var successfun = function(){
                    treeTable.render(re);
                    layer.close(index);
                };
                ZjqAdminAjax('/gincms/category/destroy?id='+ids, 'delete', {}, successfun);
            })
        }else{
            layer.msg("请选择需要删除的数据");
        }
    });

    // 刷新重载树表
    $('.refresh').click(function(){
        treeTable.render(re);
    });
    // 全部展开
    $('.open_all').click(function(){
        treeTable.openAll(re);
    });
    // 全部关闭
    $('.close_all').click(function(){
        treeTable.closeAll(re);
    });

    //树形表格事件
    treeTable.on('tree(edit)',function(data){
        editInfo(data.item);
    });

    treeTable.on('tree(del)',function(data){
        layer.confirm('确定删除此数据？',{icon:3, title:'提示信息'},function(index){
            var successfun = function(){
                treeTable.render(re);
                layer.close(index);
            };
            ZjqAdminAjax('/gincms/category/destroy?id='+data.item.id, 'delete', {}, successfun);
        });
    });

})