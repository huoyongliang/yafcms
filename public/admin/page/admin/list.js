layui.extend({
    admin: '../../js/admin'
});
layui.use(['form','layer','laydate','table','laytpl','admin'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;

    //信息列表
    var tableIns = table.render({
        elem: '#infoList',
        url : '/gincms/admin/querylist',
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limit : 15,
        limits : [10,15,20,25],
        id : "infoListTable",
        cols : [[
            {type: "checkbox", fixed:"left", width:50},
            {field: 'id', title: 'ID', width:100, align:"center"},
            {field: 'username', title: '用户登录名', width:150},
            {field: 'nickname', title: '用户昵称', width:150},
            {field: 'email', title: '邮箱'},
            {field: 'status', title: '用户状态',  align:'center',templet:"#statusBar", width:100},
            {field: 'logintime', title: '最后登录时间', templet:function(d){
                    return DateTimeFormat(d.logintime*1000,'yyyy-MM-dd hh:mm:ss');
                }, width:170},
            {field: 'createtime', title: '注册时间', align:'center', minWidth:130, templet:function(d){
                    return DateTimeFormat(d.createtime*1000,'yyyy-MM-dd hh:mm:ss');
                }, width:170},
            {title: '操作', width:200, templet:'#infoListBar',fixed:"right",align:"center"}
        ]]
    });


    //搜索
    $(".search_btn").on("click",function(){
        if($(".searchVal").val() != ''){
            table.reload("infoListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                    query: $(".searchVal").val()  //搜索的关键字
                }
            })
        }else{
            layer.msg("请输入搜索的内容");
        }
    });

    //编辑信息
    function editInfo(edit){
        var index = layui.layer.open({
            title : "编辑数据",
            area: ['90%', '90%'],
            type : 2,
            content : "edit.html",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                if(edit){
                    body.find("#id").val(edit.id);
                    body.find(".username").val(edit.username);
                    body.find(".nickname").val(edit.nickname);
                    body.find(".avatar").attr("src",edit.avatar);
                    body.find(".email").val(edit.email);
                    body.find("#group_id").attr("data-group_id",edit.group_id);
                    //body.find("dd[lay-value='"+edit.status+"']").click();
                    body.find("#status").val(edit.status);
                    form.render();
                }
                setTimeout(function(){
                    layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        });
        //layui.layer.full(index);//全屏
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            //layui.layer.full(index);
        })
    }

    $(".editInfo_btn").click(function(){
        editInfo();
    });

    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('infoListTable'),
            data = checkStatus.data,
            infoId = [];
        if(data.length > 0) {
            for (var i in data) {
                infoId.push(data[i].id);
            }
            layer.confirm('确定删除选中的数据？', {icon: 3, title: '提示信息'}, function (index) {
                var successfun = function(){
                    tableIns.reload();
                    layer.close(index);
                };
                ZjqAdminAjax('/gincms/admin/destroy?id='+infoId.join(","), 'delete', {}, successfun);
            })
        }else{
            layer.msg("请选择需要删除的数据");
        }
    })

    //列表操作
    table.on('tool(infoList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;

        if(layEvent === 'edit'){ //编辑
            editInfo(data);
        } else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此数据？',{icon:3, title:'提示信息'},function(index){
                var successfun = function(){
                    tableIns.reload();
                    layer.close(index);
                };
                ZjqAdminAjax('/gincms/admin/destroy?id='+data.id, 'delete', {}, successfun);
            });
        }
    });

})