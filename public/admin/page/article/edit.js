layui.config({
    base: '../../js/',
});
layui.use(['form','layer','layedit','upload', 'admin', 'authtree', 'laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laypage = layui.laypage,
        upload = layui.upload,
        layedit = layui.layedit,
        $ = layui.jquery;

    var authtree = layui.authtree;
    var laytpl = layui.laytpl;

    // 树状下拉菜单
    $.ajax({
        url: '/gincms/ajax/getcategory?type=article',
        dataType: 'json',
        success: function(res){
            // 如果后台返回的不是树结构，请使用 authtree.listConvert 转换
            var trees = authtree.listConvert(res.data,{});
            var article_cate_id = parseInt($("#article_cate_id").val());
            //单选框
            var selectList = authtree.treeConvertSelect(trees, {checkedKey: [article_cate_id],});
            var string =  laytpl($('#LAY-auth-tree-convert-select').html()).render({
                // 为了 layFilter 的唯一性，使用模板渲染的方式传递
                layFilter: 'cateidfilter',
                list: selectList,
            });
            $('#LAY-auth-tree-index').html(string);
            form.render('select');
            form.on('select(cateidfilter)', function(data){
                $("#article_cate_id").val(data.value);
            });
        }
    });

    //创建一个编辑器
    var editIndex = layedit.build('content',{
        height : 335,
        uploadImage : {
            url : "/gincms/ajax/upload/?utype=3",
            type: 'post'
        }
    });

    //用于同步编辑器内容到textarea
    layedit.sync(editIndex);

    var lastsrc = $(".thumbImg").attr("src");
    //上传缩略图
    upload.render({
        elem: '.thumbBox',
        url: '/gincms/ajax/upload/?lastsrc='+lastsrc,
        method : "post",  //此处是为了演示之用，实际使用中请将此删除，默认用post方式提交
        done: function(res, index, upload){
            if (res.code == 0) {
                $('.thumbImg').attr('src', res.data.src);
                $('.thumbBox').css("background", "#fff");
            } else {
                top.layer.msg(res.msg, {icon: 2, time:3000});
            }
        }
    });

    form.verify({
        title : function(val){
            if(val == ''){
                return "文章标题不能为空";
            }
        },
        content : function(val){
            layedit.sync(editIndex);
            val = layedit.getContent(editIndex);
            if(val == ''){
                return "文章内容不能为空";
            }
        }
    })

    form.on("submit(editInfo)",function(data){
        $(this).text("提交中...").attr("disabled","disabled").addClass("layui-disabled");
        //截取文章内容中的一部分文字放入文章摘要
        var description = $(".description").val();
        if (description == "") {
            description = layedit.getText(editIndex).substring(0,50)
        }

        var data = {
            title       : $(".title").val(),  //文章标题
            tag         : $(".tag").val(),  //文章标签
            description : $(".description").val(),  //文章摘要
            content     : layedit.getContent(editIndex),  //文章内容
            thumb       : $(".thumbImg").attr("src"),  //缩略图
            article_cate_id : $("#article_cate_id").val(),    //文章分类
            status      : $('.status select').val(),    //发布状态
            is_top      : data.field.isTop == "on" ? 1 : 0,    //是否置顶
        };

        var id = $("#id").val();
        var type = 'post';
        if (id>0) {
            type = 'put';
            data = $.extend(data,{id:id});
        }
        var successfun = function(){
            layer.closeAll("iframe");
            parent.location.reload();
        };
        ZjqAdminAjax('/gincms/article/save', type, data, successfun);
        $(this).text("立即提交").removeAttr("disabled").removeClass("layui-disabled");
        // // ajax提交数据
        // $.post("/article/",{
        //     title : $(".title").val(),  //文章标题
        //     description : $(".description").val(),  //文章摘要
        //     content : layedit.getContent(editIndex),  //文章内容
        //     thumb : $(".thumbImg").attr("src"),  //缩略图
        //     article_cate_id : '1',    //文章分类
        //     status : $('.status select').val(),    //发布状态
        //     is_top : data.field.isTop == "on" ? 1 : 0,    //是否置顶
        // },function(res){
        //     console.info(res);
        //     if (0 == res.code){
        //         top.layer.close(index);
        //         top.layer.msg(res.msg, {icon: 1, time:1000}, function(){
        //             layer.closeAll("iframe");
        //             //刷新父页面
        //             parent.location.reload();
        //         });
        //     } else {
        //         top.layer.msg(res.msg, {icon: 2, time:3000});
        //     }
        // });

        return false;
    })



})