layui.config({
    base: '../../js/',
});
layui.use(['form','layer','admin','authtree','laytpl'],function(){
    var form = layui.form,
    $ = layui.jquery;
    var layer = parent.layer === undefined ? layui.layer : top.layer;
    var authtree = layui.authtree;
    var laytpl = layui.laytpl;

    // 树状下拉菜单
    $.ajax({
        url: '/gincms/ajax/getauthrule',
        dataType: 'json',
        success: function(res){
            //把节点id组装成整形数组
            var rules = $("#rules").val();
            var dataStrArr=rules.split(",");//分割成字符串数组
            var ruleArr=[];//保存转换后的整型字符串
            dataStrArr.forEach(function(data,index,arr){
                ruleArr.push(+data);
            });
            // 如果后台返回的不是树结构，请使用 authtree.listConvert 转换
            var trees = authtree.listConvert(res.data,{checkedKey: ruleArr});
            //多选框
            authtree.render('#LAY-auth-tree-index', trees, {
                inputname: 'authids[]',
                layfilter: 'lay-check-auth',
                autowidth: true,
                openall:true,
                theme: 'auth-skin-default',
                themePath:'../../js/tree_themes/'
            });
            authtree.on('change(lay-check-auth)', function(data){
                // 获取所有已选中节点
                var checked = authtree.getChecked('#LAY-auth-tree-index');
                $("#rules").val(checked);
            });
        }
    });

    //提交保存

    form.on("submit(editInfo)",function(data){
        $(this).text("提交中...").attr("disabled","disabled").addClass("layui-disabled");

        var data = {
            rules   : $("#rules").val(),
            name    : $(".name").val(),
            status  : $('#status').val()
        };

        var id = $("#id").val();
        var type = 'post';
        if (id>0) {
            type = 'put';
            data = $.extend(data,{id:id});
        }
        var successfun = function(){
            layer.closeAll("iframe");
            parent.location.reload();
        };
        ZjqAdminAjax('/gincms/authgroup/save', type, data, successfun);
        $(this).text("立即提交").removeAttr("disabled").removeClass("layui-disabled");
        return false;
    });

});