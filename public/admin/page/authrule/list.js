// layui.extend({
//     admin: '../../js/admin'
// });
layui.config({
    base: '../../js/',
})
layui.use(['form','layer','table','laytpl','admin','treeTable'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table,
        treeTable = layui.treeTable;

    var	re = treeTable.render({
        icon: {
            open: 'layui-icon layui-icon-down',
            close: 'layui-icon layui-icon-right',
            left: 16,
        },
        elem: '#infoList',
        url: '/gincms/authrule/querylist',
        icon_key: 'title',
        is_checkbox: true,
        end: function(e){
            form.render();
        },
        cols: [
            {
                key: 'id',
                title: 'ID',
                width: '50px',
                align: 'center',
            },
            {
                key: 'pid',
                title: '菜单类别',
                width: '100px',
                template: function(item){
                    if(item.pid==0){
                        return '一级菜单';
                    }else if(item.pid == 1){
                        return '内容管理';
                    }else if(item.pid == 2){
                        return '用户中心';
                    }else if(item.pid == 3){
                        return '系统设置';
                    }else if(item.pid == 4){
                        return '其它类别';
                    }else{
                        return '';
                    }
                }
            },
            {
                key: 'title',
                title: '菜单名称',
                template: function(item){
                    if(item.level == 0){
                        return '<span style="color:red;">'+item.title+'</span>';
                    }else if(item.level == 1){
                        return '<span style="color:green;">'+item.title+'</span>';
                    }else if(item.level == 2){
                        return '<span style="color:#000;">'+item.title+'</span>';
                    }
                }
            },
            {
                key: 'icon',
                title: '图标',
                width: '150px',
                align: 'center',
                template: function(item){
                    return '<i class="layui-icon" style="font-size:36px">' + item.icon + '</i>';
                }
            },
            {
                key: 'name',
                title: '规则',
                width: '150px',
                align: 'left'
            },
            {
                key: 'weigh',
                title: '权重',
                width: '150px',
                align: 'center'
            },
            {
                title: '状态',
                width: '150px',
                align: 'center',
                template:function(d){
                    if(d.status == "default"){
                        return '<span class="layui-red">等待审核</span>';
                    } else if(d.status == "draft"){
                        return '<span class="layui-blue">已存草稿</span>';
                    } else {
                        return '<span class="layui-green">正常</span>';
                    }
                }
            },
            {
                title: '菜单',
                width: '100px',
                align: 'center',
                template: function(d){
                    var ismenu = d.ismenu==1 ? 'checked' : '';
                    return '<input type="checkbox" name="ismenu" lay-filter="isMenu" data-id="'+ d.id +'" lay-skin="switch" lay-text="是|否" '+ismenu+'>';
                }
            },
            {
                title: '操作',
                width: '200px',
                align: 'center',
                template: function(item){
                    return '<a class="layui-btn layui-btn-xs" lay-filter="edit">编辑</a> <a class="layui-btn layui-btn-xs layui-btn-danger" lay-filter="del">删除</a>';
                }
            }
        ]
    });


    //是否菜单
    form.on('switch(isMenu)', function(data){
        ZjqAdminAjax('/gincms/authrule/change', 'put', {ismenu:data.elem.checked ? 1 : 2,id:data.elem.dataset.id}, {});
    });

    //编辑信息
    function editInfo(edit){
        var index = layui.layer.open({
            title : "编辑数据",
            area: ['90%', '90%'],
            type : 2,
            content : "edit.html",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                //console.info(edit);
                if(edit){
                    body.find("#id").val(edit.id);
                    body.find("#pid").val(edit.pid);
                    body.find("dd[lay-value='"+edit.pid+"']").click();
                    body.find(".title").val(edit.title);
                    body.find(".name").val(edit.name);
                    body.find(".remark").val(edit.remark);
                    body.find(".icon").val(edit.icon);
                    body.find(".weigh").val(edit.weigh);
                    body.find("input[name='ismenu'][value='"+edit.ismenu+"']").next().click();
                    body.find("#status").val(edit.status);
                    body.find("dd[lay-value='"+edit.status+"']").click();
                    form.render();
                }
                setTimeout(function(){
                    layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        });
        //layui.layer.full(index);//全屏
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            //layui.layer.full(index);
        })
    }

    $(".editInfo_btn").click(function(){
        editInfo();
    });

    //批量删除
    $(".delAll_btn").click(function(){
        var ids = treeTable.checked(re).join(',');
        if(ids.length > 0) {
            layer.confirm('确定删除选中的数据？', {icon: 3, title: '提示信息'}, function (index) {
                var successfun = function(){
                    treeTable.render(re);
                    layer.close(index);
                };
                ZjqAdminAjax('/gincms/authrule/destroy?id='+ids, 'delete', {}, successfun);
            })
        }else{
            layer.msg("请选择需要删除的数据");
        }
    });

    // 刷新重载树表
    $('.refresh').click(function(){
        treeTable.render(re);
    });
    // 全部展开
    $('.open_all').click(function(){
        treeTable.openAll(re);
    });
    // 全部关闭
    $('.close_all').click(function(){
        treeTable.closeAll(re);
    });

    //树形表格事件
    treeTable.on('tree(edit)',function(data){
        editInfo(data.item);
    });

    treeTable.on('tree(del)',function(data){
        layer.confirm('确定删除此数据？',{icon:3, title:'提示信息'},function(index){
            var successfun = function(){
                treeTable.render(re);
                layer.close(index);
            };
            ZjqAdminAjax('/gincms/authrule/destroy?id='+data.item.id, 'delete', {}, successfun);
        });
    });

})