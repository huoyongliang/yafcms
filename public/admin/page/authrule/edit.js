layui.config({
    base: '../../js/',
});
layui.use(['form','layer','admin','authtree','laytpl'],function(){
    var form = layui.form,
    $ = layui.jquery;
    var layer = parent.layer === undefined ? layui.layer : top.layer;
    var authtree = layui.authtree;
    var laytpl = layui.laytpl;

// 树状下拉菜单
    $.ajax({
        url: '/gincms/ajax/getauthrule',
        dataType: 'json',
        success: function(res){
            // 如果后台返回的不是树结构，请使用 authtree.listConvert 转换
            var trees = authtree.listConvert(res.data,{});
            var pid = parseInt($("#pid").val());
            //单选框
            var selectList = authtree.treeConvertSelect(trees, {checkedKey: [pid],});
            var string =  laytpl($('#LAY-auth-tree-convert-select').html()).render({
                // 为了 layFilter 的唯一性，使用模板渲染的方式传递
                layFilter: 'pidfilter',
                list: selectList,
            });
            $('#LAY-auth-tree-index').html(string);
            form.render('select');
            form.on('select(pidfilter)', function(data){
                $("#pid").val(data.value);
            });
        }
    });

    //提交保存

    form.on("submit(editInfo)",function(data){
        $(this).text("提交中...").attr("disabled","disabled").addClass("layui-disabled");

        var data = {
            type   : $("#type").val(),
            pid    : $("#pid").val(),
            title  : $(".title").val(),
            name   : $(".name").val(),
            remark : $(".remark").val(),
            icon   : $(".icon").val(),
            weigh  : $(".weigh").val(),
            ismenu : $('input:radio[name="ismenu"]:checked').val(),
            status : $('#status').val()
        };

        var id = $("#id").val();
        var type = 'post';
        if (id>0) {
            type = 'put';
            data = $.extend(data,{id:id});
        }
        var successfun = function(){
            layer.closeAll("iframe");
            parent.location.reload();
        };
        ZjqAdminAjax('/gincms/authrule/save', type, data, successfun);
        $(this).text("立即提交").removeAttr("disabled").removeClass("layui-disabled");
        return false;
    });

});