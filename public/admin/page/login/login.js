layui.use(['form','layer','jquery'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    //登录按钮
    form.on("submit(login)",function(data){
        $(this).text("登录中...").attr("disabled","disabled").addClass("layui-disabled");
        $.post("/gincms/ajax/getuserlogin",{
            username    : $("#userName").val(),     //用户名
            password    : $("#password").val(),     //密码
            verfiycode  : $("#code").val(),         //验证码
        },function(res){
            //console.info(res);
            if (0 == res.code){
                top.layer.msg(res.msg, {icon: 1, time:1000});
                window.sessionStorage.setItem("userInfo",JSON.stringify(res.data));
                window.location.href = "/admin/";
            } else {
                top.layer.msg(res.msg, {icon: 2, time:3000});
            }
        });
        $(this).text("登录").removeAttr("disabled").removeClass("layui-disabled");
        return false;
    });

    //表单输入效果
    $(".loginBody .input-item").click(function(e){
        e.stopPropagation();
        $(this).addClass("layui-input-focus").find(".layui-input").focus();
    });
    $(".loginBody .layui-form-item .layui-input").focus(function(){
        $(this).parent().addClass("layui-input-focus");
    });
    $(".loginBody .layui-form-item .layui-input").blur(function(){
        $(this).parent().removeClass("layui-input-focus");
        if($(this).val() != ''){
            $(this).parent().addClass("layui-input-active");
        }else{
            $(this).parent().removeClass("layui-input-active");
        }
    })
});
