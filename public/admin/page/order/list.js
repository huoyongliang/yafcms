layui.config({
    base: '../../js/',
});
layui.use(['form','layer','laydate','table','laytpl','admin','soulTable'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        soulTable = layui.soulTable,
        table = layui.table;

    //信息列表
    var tableIns = table.render({
        elem: '#infoList',
        url : '/gincms/order/querylist',
        cellMinWidth : 95,
        page : true,
        autoSort: false,
        height : "full-125",
        limit : 15,
        limits : [10,15,20,25],
        id : "infoList",
        cols : [[
            {type: "checkbox", fixed:"left", width:50},
            {field: 'id', title: 'ID', width:100, align:"center"},
            {field: 'type', title: '订单类型', width:100, filter:true, sort: true},
            {field: 'title', title: '订单名称', filter:true, sort: true},
            {field: 'contacts', title: '联系人', width:150, filter:true, sort: true},
            {field: 'telephone', title: '电话', width:150, filter: {type: 'date[yyyy-MM-dd HH:mm:ss]'}, sort: true},
            {field: 'email', title: '邮箱', width:150, filter:true, sort: true},
            {field: 'status', title: '发布状态',  align:'center',templet:"#statusBar", width:100, filter:true, sort: true},
            {field: 'createtime', title: '发布时间', align:'center', minWidth:130, templet:function(d){
                return DateTimeFormat(d.createtime*1000,'yyyy-MM-dd hh:mm:ss');
            }, width:170, filter: {type: 'date[yyyy-MM-dd HH:mm:ss]'}},
            {title: '操作', width:200, templet:'#infoListBar',fixed:"right",align:"center"}
        ]]
        , filter: {
            //用于控制表头下拉显示，可以控制顺序、显示, 依次是：表格列、筛选数据、筛选条件、编辑筛选条件、导出excel
            items:['column','data','condition','editCondition','excel']
        }
        ,done: function (res, curr, count) {
            soulTable.render(this)
        }
    });

    //监听排序事件
    table.on('sort(infoList)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        //尽管我们的 table 自带排序功能，但并没有请求服务端。
        //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
        table.reload('infoList', {
            initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。
            ,where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
                orderfield: obj.field //排序字段
                ,order: obj.type //排序方式
            }
        });
    });

    //搜索
    $(".search_btn").on("click",function(){
        if($(".searchVal").val() != ''){
            table.reload("infoListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                    query: $(".searchVal").val()  //搜索的关键字
                }
            })
        }else{
            layer.msg("请输入搜索的内容");
        }
    });

    //编辑信息
    function editInfo(edit){
        var index = layui.layer.open({
            title : "编辑数据",
            area: ['90%', '90%'],
            type : 2,
            content : "edit.html",
            success : function(layero, index){
                //console.info(edit);
                var body = layui.layer.getChildFrame('body', index);
                if(edit){
                    body.find("#id").val(edit.id);
                    body.find("#type").val(edit.type);
                    //body.find("dd[lay-value='"+edit.type+"']").click();
                    body.find(".title").val(edit.title);
                    body.find(".contacts").val(edit.contacts);
                    body.find(".telephone").val(edit.telephone);
                    body.find(".email").val(edit.email);
                    body.find(".address").val(edit.address);
                    body.find(".remark").val(edit.remark);
                    //body.find("#status").find("option[value='"+edit.status+"']").prop("selected", true);
                    body.find("#status").val(edit.status);
                    //body.find("dd[lay-value='"+edit.status+"']").click();
                    //form.render();
                    var iframeWindow = layero.find('iframe')[0].contentWindow;
                    if (typeof iframeWindow.layui.form == "object"){
                        iframeWindow.layui.form.render();
                    }

                }
                setTimeout(function(){
                    layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        });
        //layui.layer.full(index);//全屏
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            //layui.layer.full(index);
        })
    }

    $(".editInfo_btn").click(function(){
        editInfo();
    });

    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('infoListTable'),
            data = checkStatus.data,
            infoId = [];
        if(data.length > 0) {
            for (var i in data) {
                infoId.push(data[i].id);
            }
            layer.confirm('确定删除选中的数据？', {icon: 3, title: '提示信息'}, function (index) {
                var successfun = function(){
                    tableIns.reload();
                    layer.close(index);
                };
                ZjqAdminAjax('/gincms/order/'+infoId.join(","), 'delete', {}, successfun);
            })
        }else{
            layer.msg("请选择需要删除的数据");
        }
    })

    //列表操作
    table.on('tool(infoList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;

        if(layEvent === 'edit'){ //编辑
            editInfo(data);
        } else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此数据？',{icon:3, title:'提示信息'},function(index){
                var successfun = function(){
                    tableIns.reload();
                    layer.close(index);
                };
                ZjqAdminAjax('/gincms/order/'+data.id, 'delete', {}, successfun);
            });
        }
    });

})