layui.config({
    base: '../../js/',
});
layui.use(['form','layer','admin','laytpl'],function(){
    var form = layui.form;
    var $ = layui.jquery;
    var layer = parent.layer === undefined ? layui.layer : top.layer;
    var laytpl = layui.laytpl;

    //提交保存

    form.on("submit(editInfo)",function(data){
        $(this).text("提交中...").attr("disabled","disabled").addClass("layui-disabled");

        var data = {
            title       : $(".title").val(),
            type        : $("#type").val(),
            contacts    : $(".contacts").val(),
            telephone   : $(".telephone").val(),
            email       : $(".email").val(),
            address     : $(".address").val(),
            remark      : $(".remark").val(),
            status      : $('#status').val()
        };

        var id = $("#id").val();
        var type = 'post';
        if (id>0) {
            type = 'put';
            data = $.extend(data,{id:id});
        }
        var successfun = function(){
            layer.closeAll("iframe");
            parent.location.reload();
        };
        ZjqAdminAjax('/gincms/order/save', type, data, successfun);
        $(this).text("立即提交").removeAttr("disabled").removeClass("layui-disabled");
        return false;
    });

});