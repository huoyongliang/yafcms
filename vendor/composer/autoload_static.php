<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2181665e0d63053958e853b1bbdb63c3
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\Models\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\Models\\' => 
        array (
            0 => __DIR__ . '/../..' . '/application/models',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2181665e0d63053958e853b1bbdb63c3::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2181665e0d63053958e853b1bbdb63c3::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
